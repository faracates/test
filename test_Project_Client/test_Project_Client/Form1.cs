﻿/*! \file Form1.cs
    \brief Soket programlama icin olusturulan c# istemci.

 Bu kod c# dilinde sunucu yazılımına paket gönderip alan bir istemci yazılımıdır
 \author Faraç Ateş
 \version   1.0.1
 \date      2017

\copyright GNU Public License.
*/

using System;
using System.Collections;
using System.Drawing;
using System.Net.Sockets;/*!< soket icin eklenen library.*/
using System.Text;
using System.Windows.Forms;

namespace test_Project_Client
{
    public partial class Form1 : Form
    {
        System.Net.Sockets.TcpClient clientSocket = new System.Net.Sockets.TcpClient();
        NetworkPack gonderilecek = new NetworkPack(); /*!<veri gönderilmesi için kullanılacak olan NetworkPack sınıfından türetilen nesne*/
        bool baglantiDurumu = false;/*!<baglanti saglaıp saglanmadıgğını tutan bool tipinde değişken*/
        int buffSize = 0;/*!<Gelen verinin boyutunu tutar*/
        public Form1()
        {
            InitializeComponent();
        }
        /*!     
      \fn Form1_Load(object sender, EventArgs e)
      \brief form yüklenirken bağlantı uyarı paneli rengini kırızı yapar ve form ismini C# client yapar

      */
        private void Form1_Load(object sender, EventArgs e)
        {
            panel1.BackColor = Color.Red;
            this.Text = "C# client";
           
            
        }
        /*!     
       \fn void button2_Click(object sender, EventArgs e)
       \brief sunucuya bağlanma işlemi yapılır
       
       */
        private void button2_Click(object sender, EventArgs e)
        {
            //!Çalışma mantığı.\n    
            //!A.Bağlantı Kurma \n  
            //!1.ip ve port değişkenlerini textboxlardan oku
            if (textBox1.Text != "" || textBox3.Text != "")
            {
                //!2.textbox1 den gelen değer ip değişkenine aktar
                String ip = textBox1.Text.ToString();
                //!3.textbox3 den gelen değer port değişkenine aktar
                int port = Convert.ToInt32(textBox3.Text);
                try
                {
                    //!4.1.baglanmayı dene.Bağlanırsa uyarı panelini yeşile boya ve baglaniDurumu değişkenine true ata
                    clientSocket.Connect(ip, port);
                    panel1.BackColor = Color.Green;
                    baglantiDurumu = true;
                }
                catch (Exception ex)
                {
                    //!4.2.Bağlanamazsa hata fırlat ve çık
                    MessageBox.Show("servera baglanilamadi hata:" + ex.Message);
                    this.Close();
                }
            }

            //!5.ip ve port alanları boşsa uyarı ver
            else MessageBox.Show("ip ve port alanlarını doldurunuz", "uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        /*!     
             \fn void button1_Click(object sender, EventArgs e)
             \brief baglantı saglandıktan sonra veri alışvrişi yapar

           */
        private void button1_Click(object sender, EventArgs e)
        {
              
            //!A.Header ve Data Değişkenlerinin Oluştur \n  
            //!1.Kullanıcıdan alınan paket 8 bayt mı kontrol et.
            label4.Text = "";
            if (baglantiDurumu)
            {
                //!1.1.Kullanıcıdan alınan paket 8 bayt değilse uyarı ver.
                while (textBox2.Text.Length < 8)
                {
                    MessageBox.Show("veri paketi 8 bayt olmalı");
                    return;
                }
                //! 2. Başlık işlemler için Header sınıfından  bir nesne türet \n                 
                Header baslik = new Header();
                //! 2.1.1 baslik baytını olustur ve comminicationId ataması yap
                baslik.ComminicationId = new BitArray(new bool[] { true, true, false });
                //! 2.1.2 reserved bitine atama yap
                baslik.Reserved = false;
                //! 2.1.3 status bitine atama yap
                baslik.Status = false;
                //! 2.1.4 code bitArray'a atama yap
                baslik.Code = new BitArray(new bool[] { true, false });
                // !2.1.5 number bitine atama yap
                baslik.Number = false;
                //!2.2.oluşturulan BitArray tipindeki değişkene Header sınıfında oluşturulan 00100011 bit dizisini aktar
                BitArray bitArray = new BitArray(baslik.MyHeader);
                //!2.3.Bu bit dizisini ConvertToByte fonksiyonuyla bayta çevir ve head değişkeninde sakla
                byte head = ConvertToByte(bitArray);
                //!2.4.NetworkPack sınıfındaki Header değişkenine oluştrurlan başlığı aktar
                gonderilecek.Baslik = head;
                //!3.Kullanıcıdan alınan data veriini bayt dizisine çevirerek NetworkPack sınıfındaki data değişkenine  aktar  
                gonderilecek.Data = Encoding.ASCII.GetBytes(textBox2.Text);

                //!B.Paket Gönderme\n  
                NetworkStream serverStream = clientSocket.GetStream();
                //!1. Oluşturulan paketi oluşturulan serverstream yardımıyla gönder
                serverStream.Write(gonderilecek.DataFrame, 0, gonderilecek.DataFrame.Length);
                serverStream.Flush();
                //!C.Paket Alma\n  
                //!1. Gelen paket boyutunu buffSize değişkeninde sakla
                buffSize = clientSocket.ReceiveBufferSize;
                //!2. Gelen paketi tutmak için instream adında byte array oluştur
                byte[] inStream = new byte[buffSize];
                //!3. okuma yap instream byte dizisine akar
                serverStream.Read(inStream, 0, buffSize);
                //!4. EKranda göstermek için byte dizisini string ifadeye çevir
                string gelen = System.Text.Encoding.Default.GetString(inStream);

                label4.Text = gelen;

                textBox2.Text = "";

                textBox2.Focus();
            }
            //!5. Bağlantı yoksa uyarı ver
            else MessageBox.Show("Servera bağlı değil!");
        }


        /*!     
       \fn textBox3_KeyPress(object sender, KeyPressEventArgs e)
       \brief port kısmına sadece sayı girilsin
       
        */
        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
            {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        /*!     \fn byte ConvertToByte(BitArray bits)
         \brief bitArray' i bayta çevir
         \param BitArray cinsinden parametre alır
         \return byte döndürür.
     */
        byte ConvertToByte(BitArray bits)
        {
            //!A.Header ve Data Değişkenlerinin Oluştur \n  
            //!1.Gelen Bit dizisi 8 bit mi kontrol et.
            if (bits.Count != 8)
            {
                //!1.1.değilse hata fırlat
                throw new ArgumentException("bits");
            }
            //!2.Copyto metodu yardımıyla bit dizisini 1 bayta çevir ve döndür
            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }
    }
    /*! \brief Bu sınıf paket bilgilerini barındırır.

Gönderilecek paketi oluşturan değişkenleri tutar.*/
    public class NetworkPack
    {
        private byte header; /*!< Paketin 1 baytlık header kısmı*/
        private byte[] data = new byte[8];  /*!< Paketin 8 baytlık data kısmı*/
        private byte[] dataFrame = new byte[9];  
        public byte[] DataFrame
        {
            get
            { 
             //!Dataframe kısmı oluştrulması için 1 baytlık header ve 8 baytlık data birleştiriliyor 
                dataFrame[0] = header;
                for (int i = 1; i < dataFrame.Length; i++)
                {
                    dataFrame[i] = data[i - 1];
                }
                return dataFrame;
            }
        } 
        public byte[] Data
        {
            set
            {
                data = value;
            }
        }
        public byte Baslik
        {
            set
            {
                header = value;
            }
        }

    }
    /*! \brief Bu sınıf paket bilgilerini barındırır.

Gönderilecek paketi oluşturan değişkenleri tutar.*/
    public class Header 
    {
        private BitArray comminicationId;
        public BitArray ComminicationId
        {
            get
            {
                return comminicationId;
            }
            set
            {
                comminicationId = value;
            }
        }
        private bool reserved;
        public bool Reserved
        {
            get
            {
                return reserved;
            }
            set
            {
                reserved = value;
            }
        }
        private bool status ;
        public bool Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
        private BitArray code ;
        public BitArray Code
        {
            get
            {
                return code;
            }
            set
            {
                code = value;
            }
        }
        private bool number ;
        public bool Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
            }
        }        
        private BitArray myheader;
        public BitArray MyHeader
        {//!header kısmı için 8 bit dizi tek tek doluduruluyor ve sonuçta  = 00100011 =38='#' baytı üretiliyor.
            get
            {
                myheader = new BitArray(8);
                myheader[0] = comminicationId[0];
                myheader[1] = comminicationId[1];
                myheader[2] = comminicationId[2];
                myheader[3] = reserved;
                myheader[4] = status;
                myheader[5] = code[0];
                myheader[6] = code[1];
                myheader[7] = number;
                return myheader;
            }
        }
    }


}
