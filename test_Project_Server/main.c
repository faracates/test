/*! \file main.c
    \brief Soket programlama icin olusturulan server.

 Bu kod c# , ve php clientlere paket gonderip alan c dilinde bir server yazilimidir.
 \author Farac Ates
 \version   1.0.0
 \date      2017

\copyright GNU Public License.
*/
#include<io.h>
#include<stdio.h>
#include<winsock2.h>
#pragma comment(lib,"ws2_32.lib")/*!< soket icin eklenen library.*/
#include<pthread.h>              /*!< thread icin eklenen library.*/

void *connection_handler(void *);/*!< connection_handler prortotip.*/
void soketOlustur();             /*!< soketOlustur fonksiyonunun prortotipi.*/
void bindIslemi();                /*!< bindIslemi fonksiyonunun prortotipi.*/

    WSADATA wsa;                  /*!<Winsock yuklendikten sonra ekleme bilgisini tutacak WSADATA structer*/
    SOCKET s ;                    /*!< olusturulacak soketi saklayacak degisken*/
    SOCKET new_socket;            /*!< yeni baglanan clientler için  kullanilacak soket*/
    struct sockaddr_in server;    /*!<icerisinde sin_family,sin_port,sin_addr ve sin_zero degiskenlerini barndiran yapidan uretilen tip*/
    struct sockaddr_in client;
    int c;                         /*!< Soket adres boyutunu tutar*/
    pthread_t thread_id;
    int gelenVeriBoyut;            /*!< paket boyutu*/
    char gelenVeri[9];             /*!< gelen paketin aktarilacagi dizi*/
    int i;

    /*! \brief Bu yapi baslik bilgilerini barindirir.

    */
   typedef struct header {
         unsigned char comminicationId :3; /*!< comminicationId 3 bitlik yer ayrildi */
         unsigned char reserved :1;        /*!< reserved 1 bitlik yer ayrildi */
         unsigned char status :1;          /*!< status 1 bitlik yer ayrildi */
         unsigned char code:2;             /*!< code 2 bitlik yer ayrildi */
         unsigned char number:1;           /*!< code 1 bitlik yer ayrildi */
         unsigned char Header:8;           /*!< Header icin 8 bitlik yer ayrildi */
} Baslik;


/*! \brief Bu yapi paket bilgilerini barindirir.

Gönderilecek paketi olusturan degiskenleri tutar.*/
  typedef struct NetworkPack{

       unsigned char data[8];       /*!< Gonderilmek istenen veri*/
       unsigned char dataFrame[9];  /*!< toplam datayi gösteren isaretci*/
     } DataPaketi;

  /*! \fn int main()
         \brief ana fonksiyon yapiss ve içerigi
         \param aldigi parametre yok.
         \return Çalisma sonucunu isletim sistemine dondurur.
     */
int main()
{
     //!Çalisma mantigi.\n
     //!A.Winsock kutuphanesini dahil et .\n
    printf("\nWinsock Dahil ediliyor..");

    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
    {
        //!A.1.Hata olustuysa uyari ver,acikla ve programi sonlandir.
        printf("Hata : %d",WSAGetLastError());
        return 1;
    }
    //!A.2.İslem basariliysa uyari ver
    printf("Basarili.\n");
    //!A.3.Soket olusturma fonksiyonu çagir.
    soketOlustur();
    //!A.3.Bind islemi fonksiyonunu çagir
    bindIslemi();
    puts("Baglantilar bekleniyor...");
    c = sizeof(struct sockaddr_in);
    //!B.2. yeni baglanan soket olup olmadigini kontrol eT.
    tekrar:
        //!B.3.yeni baglanan soket var ise new_socket degiskenini olustur ve yeni soket islemleri için kullan.
     while( (new_socket = accept(s , (struct sockaddr *)&client, &c)) != INVALID_SOCKET )
    {
        //!B.3.1 yeni baðlantý varsa uyarsi ver
        puts("baglanildi");
        //!B.4 Baðlandiktan sonraki islemler arka planda yap.Birden fazla soket için kolaylik saðlar.
       if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &new_socket) < 0)
        {
            perror("thread baslatilamadi");
            return 1;
        }
    }
    //!B.5. Baðlantý hatasi olduysa uyari ver ve çik
    if (new_socket == INVALID_SOCKET)
    {
        printf("baglanti saglanamadi hata: %d" , WSAGetLastError());
        return 1;
    }
     //!B.B.6. Baglandiktan sonra hata olduysa uyari ver ve çik
     if (new_socket < 0)
    {
        perror("baglanti koptu!");
        return 1;
    }
    goto tekrar;

    return 0;
}
/*! \brief Bu fonksiyon clientlere paket göndermek
ve almak içindir.

*/
void *connection_handler(void *socket_desc)
{    //!A.Paket Dinleme
     int sock = *(int*)socket_desc;
    DataPaketi paket;
    Baslik baslik;
     //!A.1. Gelen paketin son verisine kadar dinleme yap ve gelen paketi gelenveri içerisine kaydet.
      while( (gelenVeriBoyut = recv(sock , gelenVeri , 9 , 0)) > 0 )
    {
        //!A.1.yeni paket gelmeden paketin aktaralacagi dizi bosaltiliyor.
		gelenVeri[gelenVeriBoyut] = '\0';
        //!A.2. paketin baslik baytini "S" olarak ata. S ascii karsiligi = 83 = 0b01010011
        //!A.2.1. paketin baslik parcalari 0b01010011 baytını olusturmak uzere tek tek atandi.
        baslik.comminicationId=0b010;
        baslik.reserved=0b1;
        baslik.status=0b0;
        baslik.code=0b01;
        baslik.number=0b1;
        //!A.2.2. baslik kisimlarina atamalar yapildiktan sonra bunlar birlestiriliyor ve sonucta baslik baytı= 0b01010011
        baslik.Header=(baslik.comminicationId << 1) | baslik.reserved;
        baslik.Header=(baslik.Header << 1) | baslik.status;
        baslik.Header=(baslik.Header << 2) | baslik.code;
        baslik.Header=(baslik.Header << 1) | baslik.number;

        //!A.3. okunan veri ekrana basliyor
       printf("gelen :%s\n",gelenVeri);
       //!B.Paket Gönderme
       //!B.1.C# cliente paket gönderme
       //!B.1.1. paketin hangi clientten geldigini bul "#"->c# ,"p"->php.
       if(gelenVeri[0]=='#')
       {
           //!B.1.2. paketin ilk elemani header yap
           paket.dataFrame[0]=baslik.Header;
           //!B.1.2. paket byte dizisini doldur."Sghijklmn"
            for (i = 1; i < 9; i++)
                {
                    //!B.1.3. data dizisini doldur."ghijklmn"
                    paket.data[i-1] = (102+i);
                    paket.dataFrame[i]=paket.data[i-1];
                }
                //!B.1.4.olusturulan paketi gönder
            if( send(sock , paket.dataFrame , 9 , 0) < 0)
            {
                //!B.1.5.Paket gönderilmediyse hata ver ve çik
                puts("Veri Gonderilemedi");
                return 1;
            }
                //!B.1.6.Paket gönderildiyse ekranda göster
             printf("gonderilen :%s\n",paket.dataFrame);

       }
       //!B.2.1.php cliente paket gönderme
       if(gelenVeri[0]=='p')
       {
            //!B.2.2.paketin header kismina ata."83" yani 'S'
            paket.dataFrame[0]=baslik.Header;
            //!B.2.3. paket byte dizisini doldur."Sefghijkl"
            for (i = 1; i < 9; i++)
                {
                    //!B.2.4. data dizisini doldur."efghijkl"
                    paket.data[i-1] = (100+i);
                    paket.dataFrame[i]=paket.data[i-1];
                }
            //!B.2.5.olusturulan paketi gönder
            if( send(sock , paket.dataFrame , 9 , 0) < 0)
            {
                //!B.2.6.Paket gönderilmediyse hata ver ve çik
                puts("Veri Gonderilemedi");
                return 1;
            }
             //!B.2.7.Paket gonderildiyse ekranda goster
            printf("gonderilen :%s\n",paket.dataFrame);

       }
       //!B.2.8. paketin doldurulduðu diziyi bosalt
        memset(paket.dataFrame, 0, 9);
		memset(gelenVeri, 0, 9);

    }
    return 0;
}
 /*! \fn void soketOlustur()
         \brief soket olusturma islemleri
         \param aldigi parametre yok.
         \return deger döndürmüyor.
     */
void soketOlustur()
{
   //!A.1.1. soket olustur
   s = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP );
    if(s== INVALID_SOCKET)
    {
        //!A.1.2. hata varsa uyari ver.
        printf("soket olusturulamadi hata : %d" , WSAGetLastError());
    }
    //!A.1.2. hata yoksa "olsuturuldu" uyarisi ver.
    printf("Socket olusturuldu.\n");

}
/*!     \fn void void bindIslemi()
         \brief ip ve port belirle ve dinlemeye gecme.
         \param aldgi parametre yok.
         \return deger döndürmüyor.
     */
void bindIslemi()
{
    //!A.1.1. Ip belirle
    server.sin_family = AF_INET;
    //!A.1.2. Protokolü tcp olarak ayarla
    server.sin_addr.s_addr = INADDR_ANY;
    //!A.1.3. port numarasini ayarla
    server.sin_port = htons( 8888 );
    //!B.1.1. ip ve port bilgilerini isle.
     if( bind(s ,(struct sockaddr *)&server , sizeof(server)) == -1)
    {
        //!B.1.2. hata varsa uyari ver.
        printf("Bind tamamlanamadi hata : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    //!B.1.2. hata yoksa "basarili" uyarisi ver.
    puts("Bind basarili");
    //!B.2. Olusturulan soketten dinleme yap.
    listen(s , 3);
}
