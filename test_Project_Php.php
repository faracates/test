<!DOCTYPE html>
<html>
<head>
<style>

 .govde
{
    width:350px;
    height:auto;
    padding:100px;	
    margin:0 auto;
     border-radius:4px;
 }
.textbox
{
    width:180px;
    height:35px;
    border:1px dotted #2a595c;
    font-size:large;
    color:#545454;
}
  .buton
{
    width:100px;
    height:30px;
    background-color:#545454;
    border-radius:4px;
    color:#fff;
	margin-top:20px;
	margin-bottom:20px;
}
</style>
</head>

<body>
<?php
/*! \file test_Project_Php
    \brief Soket programlama icin olusturulan php Client.

 Bu kod sunucuya mesaj gönderen php dilinde yazılmış bir istemci yazılımıdır.
 \author Farac Ates
 \version   1.0.0
 \date      2017

\copyright GNU Public License.
*/
$gelen="";         /*!< Server 'dan gelen verileri tutan değişken.*/
$soketDurum="";    /*!< Soketin oluşturulma durumunu tutan daha sonra bunu kullanıcı arayüzünde göstermek için oluşturulan değişken.*/
$baglantiDurum=""; /*!< Soketin baglanti durumunu tutan daha sonra bunu kullanici arayüzünde göstermek için olusturulan degisken.*/
$mesajDurum="";    /*!< Mesajın gönderilme durumunu tutan daha sonra bunu kullanici arayüzünde göstermek için olusturulan degisken.*/
$labelDurum="";    /*!< 8 bayttan fazla veri girildiğinde uyarı veridirilen label.*/
$gonderilenVeri=new NetworkPack; /*!< NetworkPack snıfından turetilen ve veri gönderme isleminde kullanilacak degisken.*/
$baslik=new Header;              /*!< Header snıfından turetilen ve veri gönderme isleminde kullanilacak degisken.*/
if(array_key_exists('gonder',$_POST)){
   
	if ( ! empty($_POST['ip'])){
	//! kullanicidan alinan ip degerini degiskene ata.
    $ip = $_POST['ip'];
    }
	//! kullanicidan alinan port degerini degiskene ata.
	if ( ! empty($_POST['port'])){
    $port = $_POST['port'];
    }
	//! Veri degiskeninin uzunlugunu kontrol et 8 ise devam degilse kullaniciyi uyar.
	if(strlen($_POST["veri"])==8)
	{
	//! Veri degiskeninin uzunlugunu 8 bayt ise soketi olustur .
	if(!($sock = socket_create(AF_INET, SOCK_STREAM, 0)))
    {
	//! Soket olusturulduysa kullaniciyi bilgilendir .
      $soketDurum="Soket Olusturulamadı \n"; 
    } 
$soketDurum="Soket Olusturuldu \n"; 
    //! Girilen ip ve porta baglan.
socket_connect($sock , $ip , $port); 
    
$baglantiDurum= "baglantı kuruldu \n";
    //! headerr verisine "p" yap.
$baslik->headerr="p";
    //! data kullanicidan alinan degeri ver.
$gonderilenVeri->data=$_POST["veri"];
    //! Baslik ve datayı birlestir.
$gonderilenVeri->dataFrame=$baslik->headerr.$gonderilenVeri->data;
    //! Gönderilecek veriye olusurulan dataframe kopyala.
$gonder=$gonderilenVeri->dataFrame;

    //! Olusturulan paketi gönder.
   if( ! socket_send ( $sock , $gonder , strlen($gonder) , 0))
   {
    $baglantiDurum="Baglanılamadı";
   }    
$mesajDurum="Mesaj Gönderildi \n";
    //! Gelen paketi oku ve buf degiskeninde sakla
   if(socket_recv ( $sock , $buf , 9 , MSG_WAITALL ) === FALSE)
   {    
    $mesajDurum="Mesaj gönderilemedi \n";
   }
   //! Ekranda gösterilmek üzer buf degiskenini gelen degiskenine at
$gelen="Gelen :".$buf;
}
else $labelDurum="min 8 bayt!";
}
/*! \brief Bu sınıf gönderilecek paketin data ve dataframe değişkenlerini tutar .*/
 class NetworkPack
 {	
	public $data;
	public $dataFrame;	
 }
 /*! \brief Bu sınıf gönderilecek paketin header değişkenlerini tutar .*/
  class Header
 {
	public $headerr;
		
 }
?>

 <form method="post">
    <div>
       
        <div class="govde" style="color:#545454;  background-color:#e6f3ff; font-size:large;">

            <table>             
                
                <tr>
                    <td style="text-align: right; color:#545454; background-color:#e6f3ff; font-size:large;" >Ip Adres :</td>
                    <td>
                       <input type="text" Class="textbox" name="ip"><br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right; color:#545454; background-color:#e6f3ff; font-size:large;">Port No :</td>
                    <td>
                        <input type="text" Class="textbox" name="port"><br>
                    </td>
                </tr>
              		 <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                  <tr>
                    <td style="text-align: right; color:#545454; background-color:#e6f3ff; font-size:large;">Veri :</td>
                    <td>
                        <input type="text" maxlength="8" Class="textbox" name="veri"><br>
                    </td>
					 <td style="color:#FF0040"><label   for="other" name="lblHata"  ><?php echo $labelDurum;?></label></td>
                </tr> 
				 
                <tr>
                    <td style="text-align: right">&nbsp;</td>
                    <td>
                        <input type="submit" Class="buton" name="gonder" id="gonder" value="Gönder" /><br/>
                    </td>
                </tr>
			
				
				<tr >
                    <td style="text-align: right; color:#545454; background-color:#e6f3ff; font-size:large;">Durum :</td>
                    <td>
                        <textarea name="comment" rows="8" cols="23" ><?php echo $soketDurum."\n".$baglantiDurum."\n".$mesajDurum."\n".$gelen;?></textarea>
                    </td>
                </tr>
               				
               
            </table>

        </div>
      </div>

    </form>


</body>
</html>